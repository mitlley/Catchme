package gotcha.testapps.mitlley.test.gotcha.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import gotcha.testapps.mitlley.test.gotcha.R;
import gotcha.testapps.mitlley.test.gotcha.controller.UsuariosController;

/**
 * Created by mitlley on 02-08-17.
 */

public class CrearCuentaActivity extends AppCompatActivity {

    private Button btCrear;
    private EditText etUsername, etPassword1, etPassword2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cuenta);


        btCrear = (Button) findViewById(R.id.btCrearCuenta);
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword1 = (EditText) findViewById(R.id.etPassword1);
        etPassword2 = (EditText) findViewById(R.id.etPassword2);

        btCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Cuando se haga click en crear debemos agregar un usuario a la base de datos
                String username = etUsername.getText().toString();
                String password1 = etPassword1.getText().toString();
                String password2 = etPassword2.getText().toString();

                UsuariosController usuariosController = new UsuariosController();

                /**
                 * Tal como se especifica en UsuarioController, el metodo crearUsuario
                 * puede devolver excetions, por eso las manejamos en esta parte del codigo,
                 * mediante un try/catch
                 */
                try {

                    //linea clave, crea el usuario en la base de datos, pero puede tener errores
                    usuariosController.crearUsuario(username, password1, password2);
                    Toast.makeText(getApplicationContext(), "Usuario creado", Toast.LENGTH_SHORT).show();
                    finish();
                } catch(Exception e){

                    /**
                     * Aqui manejamos todas los errores. Pueden ser multiples, por eso los pasamos
                     * por un Switch, discriminando por el mensaje.
                     */
                    switch (e.getMessage()){
                        case "Password missmatch":

                            // Las contraseñas no coinciden
                            Toast.makeText(getApplicationContext(), "Contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            break;
                    }
                }

            }
        });
    }
}
