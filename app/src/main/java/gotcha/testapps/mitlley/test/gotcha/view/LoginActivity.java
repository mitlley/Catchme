package gotcha.testapps.mitlley.test.gotcha.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import gotcha.testapps.mitlley.test.gotcha.R;

public class LoginActivity extends AppCompatActivity {

    private TextView tvCrearCuenta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Llamamos al componente TextView presente en el xml
        tvCrearCuenta = (TextView) findViewById(R.id.tvCrearCuenta);

        // Ejecutar una accion cuando se haga click sobre el textview
        tvCrearCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Creamos un intent hacia la nueva Activity
                Intent intent = new Intent(LoginActivity.this, CrearCuentaActivity.class);
                startActivity(intent);
            }
        });
    }
}
